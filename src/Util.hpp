/*
 * Copyright (c) 2018 Yasuo Tabei
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published bytes 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef UTIL_HPP
#define UTIL_HPP

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <set>
#include <stdint.h>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include <sys/time.h>



double cos_fast(double x) {
  static double waru[5]={1.0/(3*4*5*6*7*8*9*10),-1.0/(3.0*4.0*5.0*6.0*7.0*8.0),1.0/(3.0*4.0*5.0*6.0),-1.0/(3.0*4.0),1.0};
  double y;
  double *p=waru;
  
  int i;
  //int q;
  //q=(int)(x/(2.0*PAI));
  //x=x-q*(2.0*PAI);
  x=x/32.f;
  x=x*x;
  y=-1.0/(3*4*5*6*7*8*9*10*11*12);
  do{
    y=y*x+(*p);
    p++;
  }while (p<waru+5);
  y=y*x;
  for (i=0;i<5;i++) y=y*(4.0-y);
  return 1.0-y/2.0;
}

double sin_fast(double x) {
  return cos_fast(x-M_PI/2.0);
}

double tan_fast(double x) {
  return cos_fast(x-M_PI/2.0)/cos_fast(x);
}

/*
double tan_fast2(double x) {
  double x3 = x * x * x;
  double x5 = x3 * x * x;
  
  return x + 0.3333333333333 * x + 0.13333333333 * x5;
}
*/
#endif // UTIL_HPP
